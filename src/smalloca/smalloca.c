void _smalloca(int size, void* dto, void (*wrapper)(void*, void*)) {
    void* space = __builtin_alloca(size);
    wrapper(space, dto);
}