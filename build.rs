use std::path::PathBuf;

fn main() {

    let path: PathBuf = ["src", "smalloca", "smalloca.c"].iter().collect();

    let mut config = cc::Build::new();
    config.flag("-nostdlib");

    config.file(path.to_str().unwrap());
    config.compile("smalloca")
}
